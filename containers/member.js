import React, { Component,PropTypes } from 'react';
import home from './home'
import login from './login'

import Carousel from 'react-native-snap-carousel';
import DatePicker from 'react-native-datepicker';
import CheckBox from 'react-native-icon-checkbox';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import { 
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  Button,
  Dimensions,
  NavigatorIOS,
  AsyncStorage,
  AlertIOS,
  ActivityIndicator,
  ScrollView
} from 'react-native';
var {height, width} = Dimensions.get('window');
var radio_sex = [
  {label: '男', value: 0 },
  {label: '女', value: 1 }
];
export default class member extends Component {
  constructor() {
    super();
    this.state = { 
      Account: '',
      Password:'', 
      animating: true,
      loaded: false,
      isRadioSelected: true,
      value:0,
      isGirl:false,
      isBoy:true,
      isLoveGril:false,
      isLoveBoy:true,
      isLoveBoth:false,
      isA:true,
      isB:false,
      isO:false,
      isAB:false,
      Name: '', 
      Email: '', 
      Age: 0, 
      Birthday: '', 
      Sex: '', 
      Aptitude: '',
      Constellation:'',
      Blood:'',
      Height:'',
      Weight:'',
      Address:'',
      userData:null,
    };
  }
  componentWillMount(){
      AsyncStorage.getAllKeys((err, keys) => {
      AsyncStorage.multiGet(keys, (err, stores) => {
        stores.map((result, i, store) => {
        console.log(store);
          this.setState({
            userData: JSON.parse(store[0][1]),
          });
            this._sexRadioButton(this.state.userData.Sex);
            this._aptitudeRadioButton(this.state.userData.Aptitude);
            this._bloodRadioButton(this.state.userData.Blood); 
          this.setState({
            Password:this.state.userData.Password,
            Name:this.state.userData.Name,
            Email:this.state.userData.Email,
            Birthday:this.state.userData.Birthday,
            Height:this.state.userData.Height,
            Weight:this.state.userData.Weight,
            Address:this.state.userData.Address
          });
        });
        this.setState({ 
            animating: false, 
            loaded: true,

        });
      });
    });
  }
  _sexRadioButton(sex) {
    if(sex === '男'){
      this.setState({
        isGirl:false,
        isBoy:true,
        Sex:'男'
      });
    }else{
      this.setState({
        isBoy:false,
        isGirl:true,
        Sex:'女'        
      })
    }
  }
  _aptitudeRadioButton(aptitude){
    if(aptitude === '男'){
      this.setState({
        isLoveBoy:true,
        isLoveGril:false,
        isLoveBoth:false,
        Aptitude:'男'
      })
    }else if(aptitude === '女'){
      this.setState({
        isLoveBoy:false,
        isLoveGril:true,
        isLoveBoth:false,
        Aptitude:'女'
      })
    }else{
      this.setState({
        isLoveBoy:false,
        isLoveGril:false,
        isLoveBoth:true,
        Aptitude:'雙性',        
      })
    }
  }
  _bloodRadioButton(blood){
    if(blood === 'A'){
      this.setState({
        isA:true,
        isB:false,
        isO:false,
        isAB:false,
        Blood:'A'
      });
    }else if(blood === 'B'){
      this.setState({
        isA:false,
        isB:true,
        isO:false,
        isAB:false,
        Blood:'B'        
      });
    }else if(blood === 'O'){
      this.setState({
        isA:false,
        isB:false,
        isO:true,
        isAB:false,
        Blood:'O'        
      });
    }else{
      this.setState({
        isA:false,
        isB:false,
        isO:false,
        isAB:true,
        Blood:'AB'        
      });
    }
  }
  _remove(nextRoute){
    let keys = ['userData'];
    AsyncStorage.multiRemove(keys, (err) => {
        ('Local storage user info removed!');
    });
    this.props.navigator.replacePrevious(nextRoute);
    this.props.navigator.popToTop(nextRoute);    
  }
  
  render() {
      if (!this.state.loaded) 
      return (
      <ActivityIndicator
        animating={this.state.animating}
        style={[styles.centering, {height: 80}]}
        size="large"
      />
      );
    const nextRoute = {
      component: home,
      title: '註冊',
      passProps: { myProp: 'bar',title:'123' }
    };
    const nextLogin = {
      component: login,
      title: '註冊',
      passProps: { myProp: 'bar',title:'123' }
    };
    return (
      <View style={styles.container}>
        <Image source={require('../images/home.png')} style={styles.backgroundImage}>
          <View style={styles.bigview}>
            <Carousel 
                ref={(carousel) => { this._carousel = carousel; } } 
                sliderWidth={width} 
                itemWidth={width}
                directionalLockEnabled={true}
                centerContent={true}>
              <View  >
                <ScrollView style={styles.scrollView}>
                    <Text style={styles.loginText}>
                        Member
                    </Text>
                    <View style={styles.content}>
                        <Text style={styles.columnText}>帳號</Text>                   
                        <TextInput 
                            placeholder="UserName..."          
                            placeholderTextColor="gray"
                            style={styles.textInput}
                            onChangeText={(Account) => this.setState({Account})}
                            value={this.state.userData.Account}
                            autoCapitalize="none"
                            editable={false}
                        />
                    </View>
                    <View style={styles.space}></View>
                    <View style={styles.content}>
                        <Text style={styles.columnText}>密碼</Text>
                        <TextInput 
                            placeholder="Password..."          
                            placeholderTextColor="gray"
                            style={styles.textInput}
                            onChangeText={(Password) => this.setState({Password})}
                            value={this.state.Password}
                            autoCapitalize="none"
                            secureTextEntry={true}
                            editable={false}
                            />
                    </View>
                    
                    <View style={styles.space}></View> 
                    <View style={styles.content}>
                        <Text style={styles.columnText}>姓名</Text>           
                        <TextInput 
                            placeholder="姓名..."          
                            placeholderTextColor="gray"
                            style={styles.textInput}
                            onChangeText={(Name) => this.setState({Name})}
                            value={this.state.Name}/>
                    </View>
                    <View style={styles.space}></View>
                    <View style={styles.content}>   
                      <Text style={styles.columnText}>生日</Text>                                              
                      <DatePicker
                        style={{width: 200}}
                        date={this.state.Birthday}
                        mode="date"
                        placeholder="placeholder"
                        format="YYYY/MM/DD"
                        minDate="1900/01/01"
                        maxDate="2016/06/01"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        onDateChange={(Birthday) => {this.setState({Birthday:Birthday});}}
                      />
                    </View>
                    <View style={styles.space}></View>   
                    <View style={styles.content}>
                        <Text style={styles.columnText}>信箱</Text>         
                        <TextInput 
                            placeholder="信箱..."          
                            placeholderTextColor="gray"
                            style={styles.textInput}
                            onChangeText={(Email) => this.setState({Email})}
                            value={this.state.Email}
                            autoCapitalize="none"                        
                        />
                    </View>
                        <View style={styles.radioSpace}></View>                     
                    <View style={styles.radioContent}>
                        <Text style={styles.columnRadio}>性別</Text>   
                        <View style={styles.radioView}>
                        <CheckBox
                            label="男"
                            size={30}
                            checked={this.state.isBoy}
                            onPress={() => this._sexRadioButton('男')}
                            uncheckedIconName="radio-button-unchecked"
                            checkedIconName="radio-button-checked"
                        />
                        <CheckBox
                            label="女"
                            size={30}
                            checked={this.state.isGirl}
                            onPress={() => this._sexRadioButton('女')}
                            uncheckedIconName="radio-button-unchecked"
                            checkedIconName="radio-button-checked"
                        />
                        </View>                        
                    </View>
                    <View style={styles.radioSpace}></View>                                                        
                    <View style={styles.content}>
                        <Text style={styles.columnText}>身高</Text>
                        <TextInput 
                            placeholder="身高..."          
                            placeholderTextColor="gray"
                            style={styles.textInput}
                            onChangeText={(Height) => this.setState({Height})}
                            value={this.state.Height.toString()}
                        />
                    </View>                        
                        <View style={styles.space}></View>  
                    <View style={styles.content}>
                        <Text style={styles.columnText}>體重</Text>                
                        <TextInput 
                            placeholder="體重..."          
                            placeholderTextColor="gray"
                            style={styles.textInput}
                            onChangeText={(Weight) => this.setState({Weight})}
                            value={this.state.Weight.toString()}
                        />
                    </View>                                                
                        <View style={styles.space}></View> 
                    <View style={styles.content}>
                        <Text style={styles.columnText}>居住</Text>                                
                        <TextInput 
                            placeholder="居住城市..."          
                            placeholderTextColor="gray"
                            style={styles.textInput}
                            onChangeText={(Address) => this.setState({Address})}
                            value={this.state.Address}
                        />
                    </View>         
                    <View style={styles.radioSpace}></View>                                    
                    <View style={styles.radioContent}>
                        <Text style={styles.columnRadio}>性向</Text>    
                        <View style={styles.radioView}>
                        <CheckBox
                            label="男"
                            size={30}
                            checked={this.state.isLoveBoy}
                            onPress={() => this._aptitudeRadioButton('男')}
                            uncheckedIconName="radio-button-unchecked"
                            checkedIconName="radio-button-checked"
                            onChangeText={() => this.setState({})}
                        />
                        <CheckBox
                            label="女"
                            size={30}
                            checked={this.state.isLoveGril}
                            onPress={() => this._aptitudeRadioButton('女')}
                            uncheckedIconName="radio-button-unchecked"
                            checkedIconName="radio-button-checked"
                        />
                        <CheckBox
                            label="雙性"
                            size={30}
                            checked={this.state.isLoveBoth}
                            onPress={() => this._aptitudeRadioButton('雙性')}
                            uncheckedIconName="radio-button-unchecked"
                            checkedIconName="radio-button-checked"
                        />
                        </View>
                    </View>                                          
                    <View style={styles.radioContent}>
                        <Text style={styles.columnRadio}>血型</Text>
                        <View style={styles.radioView}>
                        <CheckBox
                            label="Ａ"
                            size={30}
                            checked={this.state.isA}
                            onPress={() => this._bloodRadioButton('A')}
                            uncheckedIconName="radio-button-unchecked"
                            checkedIconName="radio-button-checked"
                        />
                        <CheckBox
                            label="B"
                            size={30}
                            checked={this.state.isB}
                            onPress={() => this._bloodRadioButton('B')}
                            uncheckedIconName="radio-button-unchecked"
                            checkedIconName="radio-button-checked"
                        />
                        <CheckBox
                            label="O"
                            size={30}
                            checked={this.state.isO}
                            onPress={() => this._bloodRadioButton('O')}
                            uncheckedIconName="radio-button-unchecked"
                            checkedIconName="radio-button-checked"
                        />
                        <CheckBox
                            label="AB"
                            size={30}
                            checked={this.state.isAB}
                            onPress={() => this._bloodRadioButton('AB')}
                            uncheckedIconName="radio-button-unchecked"
                            checkedIconName="radio-button-checked"
                        />
                        </View>
                    </View>                        
                        
                    <View style={styles.button}>
                        <Button onPress={() => { this._carousel.snapToNext(); } }
                            title=">"
                            color="white"
                            >
                        </Button>
                        <Button onPress={() => this._remove(nextLogin) }
                        title="登出"
                        color="white"
                        >
                    </Button>
                    </View>
                </ScrollView>
              </View>
              
              <View style={styles.slide1} >
                <Text style={styles.loginText}>
                        問卷
                </Text>
                    
              </View>                   
            </Carousel>
          </View>
        </Image>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',   
    flexDirection:'row',
  },
  loginText: {
    fontSize: 40,
    textAlign: 'center',
    margin: 20,
    fontFamily: 'Euphemia UCAS',
    backgroundColor:'rgba(0,0,0,0)',
  },
  
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  backgroundImage: {
    flex: 1,
    //resizeMode: 'cover', // or 'stretch'
    width: null,
    height: null,
    justifyContent:'center',
    alignItems:'center',
    // flexDirection:'row'
  },
  bigview:{
    justifyContent:'center',
    flex:1,
    flexDirection:'row',
    alignItems:'center',
    
  },
  content:{
    justifyContent:'center',
    flex:1,
    flexDirection:'row',    
  },
  radioContent:{
    // justifyContent:'center',
    flex:1,
    flexDirection:'row',    
  },
  columnText:{
    fontSize:20,
    marginRight:10,
    marginTop:5
  },
  columnRadio:{
    fontSize:20,
    marginRight:10,
    marginTop:12,
    marginLeft:5
  },
  textInput:{
    width:width*0.6,
    // alignItems: 'stretch',
    height: 30,
    borderColor: 'black', 
    borderWidth: 1,
    paddingLeft:5,
    fontFamily: 'Euphemia UCAS',
    
  },
  space:{
    height:20,
  },
  radioSpace:{
    height:10,
  },
  button:{
    paddingTop:3,
    paddingBottom:3,
    paddingLeft:10,
    paddingRight:10,
    backgroundColor:'black',
    marginTop:20,
    
  },
  slide1:{
    justifyContent:'center',
    padding:width*0.1,
    width:width,
    height:600,
    alignItems:'center',
  },
  scrollView:{
    padding:width*0.12,
    width:width,
    height:600,
  },
  radioView:{
    // flex:1,
    flexDirection:'row',
  },
});

AppRegistry.registerComponent('member', () => testlogin);
