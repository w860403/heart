import MindWaveMobile from 'react-native-mindwave-mobile';
import React, { Component } from 'react';
import quizResult from './quiz_result';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Button,
  Dimensions,
  Image
} from 'react-native';
var {height, width} = Dimensions.get('window');
const mwm = new MindWaveMobile()

export default class quiz extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            devices:[],
            //腦波數據
            delta:0,delta_max:0,delta_min:0,delta_avg:0.00,delta_sd:0.00,deltaArray:[],
            highAlpha:0,highAlpha_max:0,highAlpha_min:0,highAlpha_avg:0.00,highAlpha_sd:0.00,highAlphaArray:[],
            lowAplpha:0,lowAplpha_max:0,lowAplpha_min:0,lowAplpha_avg:0.00,lowAplpha_sd:0.00,lowAplphaArray:[],
            theta:0,theta_max:0,theta_min:0,theta_avg:0.00,theta_sd:0.00,thetaArray:[],
            lowBeta:0,lowBeta_max:0,lowBeta_min:0,lowBeta_avg:0.00,lowBeta_sd:0.00,lowBetaArray:[],
            midGamma:0,midGamma_max:0,midGamma_min:0,midGamma_avg:0.00,midGamma_sd:0.00,midGammaArray:[],
            highBeta:0,highBeta_max:0,highBeta_min:0,highBeta_avg:0.00,highBeta_sd:0.00,highBetaArray:[],
            lowGamma:0,lowGamma_max:0,lowGamma_min:0,lowGamma_avg:0.00,lowGamma_sd:0.00,lowGammaArray:[],
            //訊號
            poorSignal:200,
            //專注與冥想度
            meditation:0,
            attention:0,
            connect:false,
            disconnect:false,
            timerCounter:0,
            imageNumCounter:0,
            //測驗相關參數
            quizName:"",
            numOfpeople:0,
            numOfImage:0,
        };
        this.setState({
                quizName: this.props.quizName
            });
//----腦波接收相關----
        mwm.onFoundDevice(device => {
            //console.log(device)
            this.state.devices.push(device)
            this.setState({
                devices: this.state.devices
            });
        })
        //mwm.scan()
        this._getBrainwave(10,4)
        mwm.onConnect(state => {
            console.log(state.success=true ? "Connect Success" : "Connect Faild")
        })
        mwm.onDisconnect(state => {
            console.log(state.success=true ? "Disconnect Success" : "Disconnect Faild")
        })
        mwm.onEEGPowerDelta(data => {
            //console.log(data)
            this.setState({
                delta: data.delta,
                highAlpha: data.highAlpha,
                lowAplpha: data.lowAplpha,
                theta: data.theta,
            });
        })
        mwm.onEEGPowerLowBeta(data => {
            //console.log(data)
            this.setState({
                lowBeta: data.lowBeta,
                midGamma: data.midGamma,
                highBeta: data.highBeta,
                lowGamma: data.lowGamma,
            });
        })
        mwm.onESense(data => {
            //console.log(data)
            this.setState({
                poorSignal:data.poorSignal,
                meditation:data.meditation,
                attention:data.attention,
            })
        })
    }
//----跳頁function----
    _navigatePush(nextRoute) {
        this.props.navigator.push(nextRoute);
    }
    _navigateReplace(nextRoute){
        this.props.navigator.replace(nextRoute);
    }
//----腦波操作function----
    _devicedisconnect(){
        //console.log("Stop Connect To Device "+this.state.devices[0].id+"_"+this.state.devices[0].name)
        mwm.disconnect()
        clearTimeout(this.timer)
    }
//----腦波儲存function----
    _getBrainwave(imageNum,time){
        this.timer=setInterval(
            () => {
                if(this.state.poorSignal==0 ) {
                    this.setState({
                        timerCounter:this.state.timerCounter+1,
                    })
                    this.state.deltaArray.push(this.state.delta)
                    this.state.highAlphaArray.push(this.state.highAlpha)
                    this.state.lowAplphaArray.push(this.state.lowAplpha)
                    this.state.thetaArray.push(this.state.theta)
                    this.state.lowBetaArray.push(this.state.lowBeta)
                    this.state.midGammaArray.push(this.state.midGamma)
                    this.state.highBetaArray.push(this.state.highBeta)
                    this.state.lowGammaArray.push(this.state.lowGamma)
                    //console.log('訊號正常每秒跳一次，目前數值：'+this.state.timerCounter)
                }else{
                    //console.log('訊號不正常，請調整腦波耳機穿戴位置')
                }
                if(this.state.timerCounter%time==0 && this.state.timerCounter!=0&&this.state.poorSignal==0){
                    this.setState({
                        delta_max:this._getMax(this.state.deltaArray[0],this.state.deltaArray[1],this.state.deltaArray[2],this.state.deltaArray[3]),
                        delta_min:this._getMin(this.state.deltaArray[0],this.state.deltaArray[1],this.state.deltaArray[2],this.state.deltaArray[3]),
                        delta_sd:this._getSD(this.state.deltaArray[0],this.state.deltaArray[1],this.state.deltaArray[2],this.state.deltaArray[3]),
                        delta_avg:this._getAvg(this.state.deltaArray[0],this.state.deltaArray[1],this.state.deltaArray[2],this.state.deltaArray[3]),

                        highAlpha_max:this._getMax(this.state.highAlphaArray[0],this.state.highAlphaArray[1],this.state.highAlphaArray[2],this.state.highAlphaArray[3]),
                        highAlpha_min:this._getMin(this.state.highAlphaArray[0],this.state.highAlphaArray[1],this.state.highAlphaArray[2],this.state.highAlphaArray[3]),
                        highAlpha_sd:this._getSD(this.state.highAlphaArray[0],this.state.highAlphaArray[1],this.state.highAlphaArray[2],this.state.highAlphaArray[3]),
                        highAlpha_avg:this._getAvg(this.state.highAlphaArray[0],this.state.highAlphaArray[1],this.state.highAlphaArray[2],this.state.highAlphaArray[3]),

                        lowAplpha_max:this._getMax(this.state.lowAplphaArray[0],this.state.lowAplphaArray[1],this.state.lowAplphaArray[2],this.state.lowAplphaArray[3]),
                        lowAplpha_min:this._getMin(this.state.lowAplphaArray[0],this.state.lowAplphaArray[1],this.state.lowAplphaArray[2],this.state.lowAplphaArray[3]),
                        lowAplpha_sd:this._getSD(this.state.lowAplphaArray[0],this.state.lowAplphaArray[1],this.state.lowAplphaArray[2],this.state.lowAplphaArray[3]),
                        lowAplpha_avg:this._getAvg(this.state.lowAplphaArray[0],this.state.lowAplphaArray[1],this.state.lowAplphaArray[2],this.state.lowAplphaArray[3]),

                        theta_max:this._getMax(this.state.thetaArray[0],this.state.thetaArray[1],this.state.thetaArray[2],this.state.thetaArray[3]),
                        theta_min:this._getMin(this.state.thetaArray[0],this.state.thetaArray[1],this.state.thetaArray[2],this.state.thetaArray[3]),
                        theta_sd:this._getSD(this.state.thetaArray[0],this.state.thetaArray[1],this.state.thetaArray[2],this.state.thetaArray[3]),
                        theta_avg:this._getAvg(this.state.thetaArray[0],this.state.thetaArray[1],this.state.thetaArray[2],this.state.thetaArray[3]),

                        lowBeta_max:this._getMax(this.state.lowBetaArray[0],this.state.lowBetaArray[1],this.state.lowBetaArray[2],this.state.lowBetaArray[3]),
                        lowBeta_min:this._getMin(this.state.lowBetaArray[0],this.state.lowBetaArray[1],this.state.lowBetaArray[2],this.state.lowBetaArray[3]),
                        lowBeta_sd:this._getSD(this.state.lowBetaArray[0],this.state.lowBetaArray[1],this.state.lowBetaArray[2],this.state.lowBetaArray[3]),
                        lowBeta_avg:this._getAvg(this.state.lowBetaArray[0],this.state.lowBetaArray[1],this.state.lowBetaArray[2],this.state.lowBetaArray[3]),

                        midGamma_max:this._getMax(this.state.midGammaArray[0],this.state.midGammaArray[1],this.state.midGammaArray[2],this.state.midGammaArray[3]),
                        midGamma_min:this._getMin(this.state.midGammaArray[0],this.state.midGammaArray[1],this.state.midGammaArray[2],this.state.midGammaArray[3]),
                        midGamma_sd:this._getSD(this.state.midGammaArray[0],this.state.midGammaArray[1],this.state.midGammaArray[2],this.state.midGammaArray[3]),
                        midGamma_avg:this._getAvg(this.state.midGammaArray[0],this.state.midGammaArray[1],this.state.midGammaArray[2],this.state.midGammaArray[3]),

                        highBeta_max:this._getMax(this.state.highBetaArray[0],this.state.highBetaArray[1],this.state.highBetaArray[2],this.state.highBetaArray[3]),
                        highBeta_min:this._getMin(this.state.highBetaArray[0],this.state.highBetaArray[1],this.state.highBetaArray[2],this.state.highBetaArray[3]),
                        highBeta_sd:this._getSD(this.state.highBetaArray[0],this.state.highBetaArray[1],this.state.highBetaArray[2],this.state.highBetaArray[3]),
                        highBeta_avg:this._getAvg(this.state.highBetaArray[0],this.state.highBetaArray[1],this.state.highBetaArray[2],this.state.highBetaArray[3]),

                        lowGamma_max:this._getMax(this.state.lowGammaArray[0],this.state.lowGammaArray[1],this.state.lowGammaArray[2],this.state.lowGammaArray[3]),
                        lowGamma_min:this._getMin(this.state.lowGammaArray[0],this.state.lowGammaArray[1],this.state.lowGammaArray[2],this.state.lowGammaArray[3]),
                        lowGamma_sd:this._getSD(this.state.lowGammaArray[0],this.state.lowGammaArray[1],this.state.lowGammaArray[2],this.state.lowGammaArray[3]),
                        lowGamma_avg:this._getAvg(this.state.lowGammaArray[0],this.state.lowGammaArray[1],this.state.lowGammaArray[2],this.state.lowGammaArray[3]),
                    })
                    //在這邊把腦波存入資料庫
                    console.log({"deltamax":this.state.delta_max,"delta_min":this.state.delta_min,"delta_avg":this.state.delta_avg,"delta_sd":this.state.delta_sd})
                    console.log({"highAlpha_max":this.state.highAlpha_max,"highAlpha_min":this.state.highAlpha_min,"highAlpha_avg":this.state.highAlpha_avg,"highAlpha_sd":this.state.highAlpha_sd})
                    console.log({"lowAplpha_max":this.state.lowAplpha_max,"lowAplpha_min":this.state.lowAplpha_min,"lowAplpha_avg":this.state.lowAplpha_avg,"lowAplpha_sd":this.state.lowAplpha_sd})
                    console.log({"theta_max":this.state.theta_max,"theta_min":this.state.theta_min,"theta_avg":this.state.theta_avg,"theta_sd":this.state.theta_sd})
                    console.log({"lowBeta_max":this.state.lowBeta_max,"lowBeta_min":this.state.lowBeta_min,"lowBeta_avg":this.state.lowBeta_avg,"lowBeta_sd":this.state.lowBeta_sd})
                    console.log({"midGamma_max":this.state.midGamma_max,"midGamma_min":this.state.midGamma_min,"midGamma_avg":this.state.midGamma_avg,"midGamma_sd":this.state.midGamma_sd})
                    console.log({"highBeta_max":this.state.highBeta_max,"highBeta_min":this.state.highBeta_min,"highBeta_avg":this.state.highBeta_avg,"highBeta_sd":this.state.highBeta_sd})
                    console.log({"lowGamma_max":this.state.lowGamma_max,"lowGamma_min":this.state.lowGamma_min,"lowGamma_avg":this.state.lowGamma_avg,"lowGamma_sd":this.state.lowGamma_sd})
                    var imagecount = this.state.imageNumCounter+1
                    console.log("-----------這是第"+imagecount+"張圖片的成績------------")
                    this.setState({
                        deltaArray:[],
                        highAlphaArray:[],
                        lowAplphaArray:[],
                        thetaArray:[],
                        lowBetaArray:[],
                        midGammaArray:[],
                        highBetaArray:[],
                        lowGammaArray:[],
                        imageNumCounter:this.state.imageNumCounter+1
                    })
                }
                if(this.state.imageNumCounter==imageNum){
                    clearTimeout(this.timer)
                    const nextQuizresult = {
                        component: quizResult,
                        title: '測驗結果',
                        passProps: { myProp: 'bar',title:'123' }
                    };
                    this._devicedisconnect()
                    this._navigateReplace(nextQuizresult);
                }else{
                    
                }
                // console.log("timercounter:"+this.state.timerCounter)
                // console.log("imageNumCounter:"+this.state.imageNumCounter)
            },1000
        );
    }
//----腦波運算function----
    //取得最大值（傳入四個值,回傳最大值）
    _getMax(data1,data2,data3,data4){
        var dataMax = data1;
        dataMax<data2 ? dataMax=data2 : dataMax=dataMax
        dataMax<data3 ? dataMax=data3 : dataMax=dataMax
        dataMax<data4 ? dataMax=data4 : dataMax=dataMax
        //console.log(dataMax)
        return dataMax
    }
    //取得最小值（傳入四個值，回傳最小值）
    _getMin(data1,data2,data3,data4){
        var dataMin = data1;
        dataMin>data2 ? dataMin=data2 : dataMin=dataMin
        dataMin>data3 ? dataMin=data3 : dataMin=dataMin
        dataMin>data4 ? dataMin=data4 : dataMin=dataMin
        //console.log(dataMin)
        return dataMin
    }
    //計算平均（傳入四個值，回傳平均值）
    _getAvg(data1,data2,data3,data4){
        //console.log(dataAvg)
        return ((data1+data2+data3+data4)/4).toFixed(2)
    }
    //計算標準差（傳入四個值，回傳標準差）
    _getSD(data1,data2,data3,data4){
        const average = (data1+data2+data3+data4)/4
        //console.log("average="+average)
        return (Math.sqrt((((data1-average)*(data1-average)
        +(data2-average)*(data2-average)
        +(data3-average)*(data3-average)
        +(data4-average)*(data4-average))/4))).toFixed(2)
    }
  render() {
      const deviceFirst = this.state.devices[0]
    return (
      <View style={styles.container}>
        <Image source={require('../images/home.png')} style={styles.backgroundImage}>
            <View style={styles.bigview}>
                <Image source={{uri: 'http://163.17.135.185/Hearting/Upload/chen.jpg'}} style={{width: width, height: height}}/>
            </View>
        </Image>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'row'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  instructions2:{
    textAlign: 'center',
    color: 'red',
    marginBottom: 5,
  },
  button:{
    backgroundColor:'#000000',  
  },  
  bigview:{
    justifyContent:'center',
    flex:1,
    alignItems:'center',
  },
  backgroundImage: {
    flex: 1,
    //resizeMode: 'cover', // or 'stretch'
    width: null,
    height: null,
    justifyContent:'center',
    alignItems:'center',
    // flexDirection:'row'
  },
});

