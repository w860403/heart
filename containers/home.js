import React, { Component,PropTypes } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  NavigatorIOS,
  TouchableHighlight,
  Dimensions,
  Button,
  Image,
  TextInput,
  AlertIOS,
  AsyncStorage,
  ActivityIndicator
} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import login from './login';
import member from './member';
import quiz from './quiz';
import midwaveconnect from './mindwaveconnect'

let value;  
var {height, width} = Dimensions.get('window');
export default class Heart extends Component {
  constructor() {
    super();
    this.state = { 
      animating: true,
      loaded: false,
      userData:null,
    };
  }
  async _getStorage(){
    try{
    AsyncStorage.getAllKeys((err, keys) => {
        console.log(1);
      AsyncStorage.multiGet(keys, (err, stores) => {
        console.log(2);
        stores.map((result, i, store) => {
        console.log(3);
        console.log(store);
          this.setState({
                userData: JSON.parse(store[0][1])
            });
        });
            this.setState({ animating: false, loaded: true });
      });
    });
  }
  catch(error){
    console.log("error");
  }
  }
  componentWillMount(){
    console.log(0);
    this._getStorage().done();
  }
   render() {
    if (!this.state.loaded) 
      return (
      <ActivityIndicator
        animating={this.state.animating}
        style={[styles.centering, {height: 80}]}
        size="large"
      />
      );
     console.log(4);
      if(this.state.userData==null){
        return (
          <NavigatorIOS 
            navigationBarHidden={true}
            interactivePopGestureEnabled={true}
            initialRoute={{
              component: login,
              title: '',
              passProps: { myProp: 'foo' }
            }}
            style={{flex: 1}}
          />
        );
      }else{
        return (
          <NavigatorIOS 
            navigationBarHidden={true}
            interactivePopGestureEnabled={true}            
            initialRoute={{
              component: home,
              title: '',
              passProps: { myProp: 'foo' }
            }}
            style={{flex: 1}}
          />
        );
      }
  }
}

class home extends Component {
  constructor() {
    super();
    this.state = { 
      userData:{},
      isLogin:false      
    };
  }
  _handleBackPress() {
    this.props.navigator.pop();
  }
//----跳頁function----
    _navigatePush(nextRoute) {
        this.props.navigator.push(nextRoute);
    }
    _navigateReplace(nextRoute){
        this.props.navigator.replace(nextRoute);
    }
  // _remove(){
  //   let keys = ['account', 'password'];
  //   AsyncStorage.multiRemove(keys, (err) => {
  //   ('Local storage user info removed!');
  //   });
  // }
  componentWillMount(){
      AsyncStorage.getAllKeys((err, keys) => {
        console.log(1);
      AsyncStorage.multiGet(keys, (err, stores) => {
        console.log(2);
        stores.map((result, i, store) => {
        console.log(3);
        console.log(store);
          this.setState({
                userData: JSON.parse(store[0][1])
            });
            console.log(this.state.userData.Name);
        });
      });
    });
  }
  _remove(nextRoute){
    let keys = ['userData'];
    AsyncStorage.multiRemove(keys, (err) => {
        ('Local storage user info removed!');
    }); 
  }
  render() {
    const nextLogin = {
      component: login,
      title: '登入',
      passProps: { myProp: 'bar',title:'123' }
    };
    const nextMember = {
      component: member,
      title: '會員',
      passProps: { myProp: 'bar',title:'123' }
    };
    const nextMindWaveConnect = {
      component: midwaveconnect,
      title: '連接腦波耳機',
      passProps: { myProp: 'bar',title:'123' }
    };
    
    return(
      <View style={styles.container}>
          <Image source={require('../images/home.png')} style={styles.backgroundImage}>
      
          <View style={styles.bigview}>
                
            <Carousel 
              ref={'carousel'} 
              sliderWidth={width} 
              itemWidth={width*0.7+width*0.035*2} 
              directionalLockEnabled={true}
              centerContent={true}
              horizontal={true}>
              <View style={styles.slide1} >
                <View >
                  <Image source={require('../images/handsome01.jpg')} style={styles.cricle}/>
                </View>
                <Text style={styles.textView}>Hello {this.state.userData.Name}</Text>
                <View style={styles.button}>
                  <Button onPress={() => this._navigatePush(nextMember) }
                    title="Member"
                    color="#FFFFFF"
                    fontFamily="Trebuchet MS"
                    >
                  </Button>
                </View>
              </View>
              <View style={styles.slide1} >
                <View style={styles.button}>
                  <Button onPress={() => this._navigateReplace(nextMindWaveConnect) }
                    title="素人照片測驗"
                    color="#FFFFFF"
                    fontFamily="Trebuchet MS"
                    >
                  </Button>
                </View>
              </View>
              <View style={styles.slide1} >
                <Text style={styles.text}>Slide3</Text>
              </View>
              <View style={styles.slide1} >
                <Text style={styles.text}>Slide4</Text>
              </View>
            </Carousel>
          </View>
        </Image>
        </View>
      );
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'row'
  },
  cricle:{
    height: 120,
    width: 120,
    borderRadius: 60,
    marginBottom:20
  },
  backgroundImage: {
    flex: 1,
    //resizeMode: 'cover', // or 'stretch'
    width: null,
    height: null,
    justifyContent:'center',
    alignItems:'center',
    // flexDirection:'row'
  },
  bigview:{
    justifyContent:'center',
    flex:1,
    flexDirection:'row',
    alignItems:'center',
  },
  slide1:{
    justifyContent:'center',
    backgroundColor: 'rgba(52, 52, 52, 0.3)',
    margin:width*0.035,
    width:width*0.7,
    height:400,
    alignItems:'center',
  },
  textView:{
    marginBottom:25,
    fontSize:35,
    fontFamily: 'Euphemia UCAS',
  },
  button:{
    paddingTop:3,
    paddingBottom:3,
    paddingLeft:10,
    paddingRight:10,
    backgroundColor:'#A03066',  
  }
});
AppRegistry.registerComponent('Heart', () => Heart);
