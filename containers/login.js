import React, { Component,PropTypes } from 'react';
import home from './home';
import register from './register';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  Button,
  Dimensions,
  NavigatorIOS,
  AsyncStorage,
  AlertIOS,
  ActivityIndicator,

} from 'react-native';
var {height, width} = Dimensions.get('window');

export default class login extends Component {
  constructor() {
    super();
    this.state = { 
      Account: '',
      Password:'', 
      animating: true,
      loaded: false,
      loginSuccess:false
    };
  }
  _login(Account,Password,nextRoute) {
    
    var userData="";
    var self=this;
    if(Account==="" || Password===""){
        return AlertIOS.alert("請輸入欄位");
      }
      else{
         fetch('http://163.17.135.185/Hearting/api/Memberapi/Read', {
            method: 'POST',
            headers: {
            //'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              "Account":Account,
              "Password": Password
            })
          })
          .then((response) => response.json())
          .then((responseData) => {
            if(responseData.status==false){
              return AlertIOS.alert("無此帳號");
            }else{
              userData=JSON.stringify(responseData.user);
              AsyncStorage.multiSet([
              ["userData", userData]
            ])
              self._goToHome(nextRoute);
            }
            console.log(JSON.stringify(responseData));
            
            console.log(responseData.user);
          //   AlertIOS.alert(
          //     "POST Response",
          //     "Response Body -> " + JSON.stringify(responseData)
          // )
            
            // self.setState({ animating: false, loaded: true });
          })
      }
  }
  _goToHome(nextRoute){
    // if (!this.state.loaded) 
    //   return (
    //   <ActivityIndicator
    //     animating={this.state.animating}
    //     style={[styles.centering, {height: 80}]}
    //     size="large"
    //   />
    //   );
    this.props.navigator.replace(nextRoute);
  }
  _goToRegister(nextRegister){
    this.props.navigator.push(nextRegister);
  }
  
  render() {
    const nextRoute = {
      component: home,
      title: '登入',
      passProps: { myProp: 'bar',title:'123' }
    };
    const nextRegister = {
      component: register,
      title: 'Register',
      passProps: { myProp: 'bar',title:'123' }
    };
        return (
        <View style={styles.container}>
          <Image source={require('../images/home.png')} style={styles.backgroundImage}>
            <View style={styles.bigview}>
              <Text style={styles.loginText}>
                Login
              </Text>
              <TextInput 
                placeholder="UserName..."
                placeholderTextColor="gray"
                style={styles.textInput}
                onChangeText={(Account) => this.setState({Account})}
                value={this.state.Account}
                autoCapitalize="none"
              />
              <View style={styles.space}></View>
              <TextInput 
                placeholder="Password..."          
                placeholderTextColor="gray"
                style={styles.textInput}
                onChangeText={(Password) => this.setState({Password})}
                value={this.state.Password}
                autoCapitalize="none"
                secureTextEntry={true}
              />
              <View style={styles.button}>
                    <Button onPress={() => this._login(this.state.Account,this.state.Password,nextRoute) }
                      title="Sign In"
                      color="white"
                      >
                    </Button>              
                  </View>
                  <View style={styles.button}>
                    <Button onPress={() => this._goToRegister(nextRegister) }
                      title="Register"
                      color="white"
                      >
                    </Button>
                  </View>
            </View>
          </Image>
        </View>
      );
    }
  }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',   
    flexDirection:'row',
  },
  loginText: {
    fontSize: 40,
    textAlign: 'center',
    margin: 20,
    fontFamily: 'Euphemia UCAS',
    backgroundColor:'rgba(0,0,0,0)',
  },
  
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  backgroundImage: {
    flex: 1,
    //resizeMode: 'cover', // or 'stretch'
    width: null,
    height: null,
    justifyContent:'center',
    alignItems:'center',
    // flexDirection:'row'
  },
  bigview:{
    justifyContent:'center',
    flex:1,
    alignItems:'center',
    width:width*0.6,    
  },
  textInput:{
    // width:200,
    height: 30,
    borderColor: 'black', 
    borderWidth: 1,
    paddingLeft:5,
    fontFamily: 'Euphemia UCAS',
    
  },
  space:{
    height:20,
  },
  button:{
    paddingTop:3,
    paddingBottom:3,
    paddingLeft:10,
    paddingRight:10,
    backgroundColor:'black',
    marginTop:20,
    
  }
});

