import React, { Component } from 'react';
import MindWaveMobile from 'react-native-mindwave-mobile';
import quiz from './quiz';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  Image,
  Button
} from 'react-native';
const mwm = new MindWaveMobile()

export default class mindwaveConnect extends Component {
    constructor() {
        super();
        this.state = { 
            animating: true,
            deviceFound:false,
            mindwaveConnected: false,
            devices:[],
            connect:false,
            disconnect:false,
            timerCounter:0,
            imageNumCounter:1
        };
//----腦波接收相關----
        mwm.onFoundDevice(device => {
            console.log(device)
            this.state.devices.push(device)
            this.setState({
                deviceFound:true
            });
            if(this.state.deviceFound && !this.state.mindwaveConnected){
                this._deviceconnect()
            }
        })
        mwm.scan()
        mwm.onConnect(state => {
            console.log(state.success=true ? "Connect Success" : "Connect Faild")
            if(!this.state.mindwaveConnected){
                this.setState({
                    mindwaveConnected:true
                });
            }
        })
        mwm.onDisconnect(state => {
            console.log(state.success=true ? "Disconnect Success" : "Disconnect Faild")
            if(this.state.mindwaveConnected){
                this.setState({
                    mindwaveConnected:false
                });
            }
        })
    }
//----跳頁function----
  _navigatePush(nextRoute) {
    this.props.navigator.push(nextRoute);
  }
  _navigateReplace(nextRoute){
    this.props.navigator.replace(nextRoute);
  }
//----腦波操作function----
     _devicescan(){
        mwm.scan()
    }
    _deviceconnect(){
        console.log("Try To Connect To Device "+this.state.devices[0].id+"_"+this.state.devices[0].name)
        mwm.connect(this.state.devices[0].id)
    }
    _devicedisconnect(){
        console.log("Stop Connect To Device "+this.state.devices[0].id+"_"+this.state.devices[0].name)
        mwm.disconnect()

    }
  render() {
      const nextQuiz = {
      component: quiz,
      title: '測驗畫面',
      passProps: { }
    };
    //如果還沒連接，跳連接中&Loading
      if(!this.state.mindwaveConnected){
        return (
        <View style={styles.container}>
            <Image source={require('../images/home.png')} style={styles.backgroundImage}>
                <View style={styles.bigview}>
                    <View style={styles.LoadingTextView}>
                        <Text style={styles.BigText}>腦波耳機連接中...</Text>
                    </View>
                        <ActivityIndicator
                        animating={this.state.animating}
                        style={[styles.centering, {height: 80}]}
                        size="large"
                        />
                    <View style={styles.button}>
                        <Button  onPress={() => this._deviceconnect() }
                        title="連結"
                        color="#FFFFFF"
                        fontFamily="Trebuchet MS">
                        </Button>
                    </View>
                </View>
            </Image>
        </View>
        );
      }
      //如果沒有Found Device
      else if(!this.state.deviceFound){
        return (
        <View style={styles.container}>
            <Image source={require('../images/home.png')} style={styles.backgroundImage}>
                <View style={styles.bigview}>
                    <View style={styles.LoadingTextView}>
                        <Text style={styles.MideumText}>請正確使用藍牙連接腦波耳機</Text>
                    </View>
                        <ActivityIndicator
                        animating={this.state.animating}
                        style={[styles.centering, {height: 80}]}
                        size="large"
                        />
                </View>
            </Image>
        </View>
        );
      }
      //如果連接到腦波耳機了
      else{
          const deviceFirst = this.state.devices[0]
          return (
          <View style={styles.container}>
            <Image source={require('../images/home.png')} style={styles.backgroundImage}>
                <View style={styles.bigview}>
                    <Text style={styles.welcome}>
                    已連接至：
                    {deviceFirst ? deviceFirst.id : null}_{deviceFirst ? deviceFirst.name : null}
                    </Text>
                    <View style={styles.button}>
                        <Button style={styles.cricle} onPress={() => this._navigateReplace(nextQuiz) }
                        title="開始測驗"
                        color="#FFFFFF"
                        fontFamily="Trebuchet MS"
                        >
                        </Button>
                    </View>
                </View>
            </Image>
        </View>
          )
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'row'
  },
  backgroundImage: {
    flex: 1,
    //resizeMode: 'cover', // or 'stretch'
    width: null,
    height: null,
    justifyContent:'center',
    alignItems:'center',
    // flexDirection:'row'
  },  
  bigview:{
    justifyContent:'center',
    flex:1,
    flexDirection:'row',
    alignItems:'center',
  },
  BigText:{
    fontSize:35,
    fontFamily: 'Euphemia UCAS',
  },
  MideumText:{
      fontSize:20,
    fontFamily: 'Euphemia UCAS',
  },
  LoadingTextView:{
    backgroundColor: 'rgba(52, 52, 52, 0)',
  },
  cricle:{
    height: 120,
    width: 120,
    borderRadius: 60,
    backgroundColor:'#000000', 
  },
  button:{
    backgroundColor:'#000000',  
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});

