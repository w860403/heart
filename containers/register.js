import React, { Component,PropTypes } from 'react';
import home from './home'
import Carousel from 'react-native-snap-carousel';
import CheckBox from 'react-native-icon-checkbox';
import DatePicker from 'react-native-datepicker';
import homefunction from '../functions/homefunction';
var HomeFunction = new homefunction()
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import { 
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  Button,
  Dimensions,
  NavigatorIOS,
  AsyncStorage,
  AlertIOS,
  ActivityIndicator,
  DatePickerIOS,
  ScrollView
} from 'react-native';
var {height, width} = Dimensions.get('window');
var M_myStyle = [
  {label: '陽光', value: 0 },
  {label: '文青', value: 1 },
  {label: '宅男', value: 2 },
  {label: '草食男', value: 3 },
  {label: '霸氣', value: 4 }
];
var W_myStyle = [
  {label: '可愛', value: 0 },
  {label: '性感', value: 1 },
  {label: '氣質', value: 2 },
  {label: '腐女', value: 3 },
  {label: '肉食女', value: 4 },
  {label: '天然', value: 5 }
];
var faceType = [
  {label: '國字臉', value: 0 },
  {label: '圓臉', value: 1 },
  {label: '瓜子臉', value: 2 },
  {label: '鵝蛋臉', value: 3 },
  {label: '長臉', value: 4 },
];
var decoration = [
  {label: '戴眼鏡', value: 0 },
  {label: '放大片', value: 1 },
  {label: '項鍊', value: 2 },
  {label: '耳環', value: 3 },
  {label: '髮帶', value: 4 },
  {label: '帽子', value: 5 },  
];
var M_hairStyle = [
  {label: '微卷暖男造型', value: 0 },
  {label: '二分區式造型', value: 1 },
  {label: '後吹露額造型', value: 2 },
  {label: '分線油頭造型', value: 3 },
  {label: '平瀏海造型', value: 4 },
];
var W_hairStyle = [
  {label: '極短髮', value: 0 },
  {label: '短髮', value: 1 },
  {label: '長髮', value: 2 },
  {label: '咩咩頭', value: 3 },
  {label: '空氣瀏海', value: 4 },
  {label: '斜劉海', value: 5 },  
  {label: '中分', value: 6 },  
  {label: '小卷', value: 7 },  
  {label: '大波浪', value: 8 },  
  {label: '微捲', value: 9 },  
  {label: '內彎', value: 10 },  
  {label: '直髮', value: 11 },  
  {label: '離子燙', value: 12 },  
  {label: '馬尾', value: 13 },  
];
var isMakeup = [
  {label: '有化妝', value: 0 },
  {label: '沒有化妝', value: 1 },
];
var eyes = [
  {label: '大眼睛', value: 0 },
  {label: '小眼睛', value: 1 },
  {label: '鳳眼', value: 2 },
  {label: '圓眼', value: 3 },
];
var eyelid = [
  {label: '單眼皮', value: 0 },
  {label: '雙眼皮', value: 1 },
  {label: '內雙', value: 2 },
];
var eyebrow = [
  {label: '一字眉', value: 0 },
  {label: '八字眉', value: 1 },
  {label: '粗眉', value: 2 },
];
var chest = [
  {label: 'A~B', value: 0 },
  {label: 'C~D', value: 1 },
  {label: 'E以上', value: 2 },
];
var stature = [
  {label: '圓胖西洋梨型', value: 0 },
  {label: '精瘦小黃瓜型', value: 1 },
  {label: '肌肉磚塊行型', value: 2 },
  {label: '扁寬倒三角形', value: 3 },
  {label: '乾扁豌豆型', value: 4 },
];
var W_height = [
  {label: '150以下', value: 0 },
  {label: '150~155', value: 1 },
  {label: '156~160', value: 2 },
  {label: '161~165', value: 3 },
  {label: '166~170', value: 4 },
  {label: '170以上', value: 5 },  
];
var M_height = [
  {label: '165以下', value: 0 },
  {label: '165~170', value: 1 },
  {label: '171~175', value: 2 },
  {label: '176~180', value: 3 },
  {label: '181~185', value: 4 },
  {label: '185以上', value: 5 },  
];
var W_wear = [
  {label: '氣質', value: 0 },
  {label: '可愛', value: 1 },
  {label: '個性', value: 2 },
  {label: '性感', value: 3 },
];
var M_wear = [
  {label: '文青', value: 0 },
  {label: '休閒', value: 1 },
  {label: '運動', value: 2 },
  {label: '西裝', value: 3 },
];
var pose = [
  {label: '休閒', value: 0 },
  {label: '回眸', value: 1 },
  {label: '全身', value: 2 },
  {label: '撥髮', value: 3 },
  {label: '搞怪', value: 4 },
];
var firstSee = [
  {label: '臉', value: 0 },
  {label: '胸', value: 1 },
  {label: '腿', value: 2 },
  {label: '其他', value: 3 },
];
var likeAge = [
  {label: '小五歲以上', value: 0 },
  {label: '小2~5歲', value: 1 },
  {label: '差兩歲左右', value: 2 },
  {label: '大2~5歲', value: 3 },
  {label: '大5歲以上', value: 4 },
];

export default class register extends Component {

  //state變數
  constructor() {
    super();
    let maxDate = new Date('1990/01/01');
    let now=new Date();
    console.log(123);
    this.state = { 
      Account: '',
      Password:'', 
      animating: true,
      loaded: false,
      isRadioSelected: true,
      value:0,
      isGirl:false,
      isBoy:true,
      isLoveGril:false,
      isLoveBoy:true,
      isLoveBoth:false,
      isA:true,
      isB:false,
      isO:false,
      isAB:false,
      Name: '', 
      Email: '', 
      Age: 0, 
      Birthday: now, 
      Sex: '男', 
      Aptitude: '男',
      Constellation:'',
      Blood:'Ａ',
      Height:'',
      Weight:'',
      Address:'',
      checkPassword:'',
      minDate:maxDate,

      Decorations:'',
      M_Hairstyle:'', 
      W_Hairstyle:'',
      isMakeup:'',
      Eyes:'',
      Eyelid:'',
      Eyebrow:'',
      Chest:'',
      Stature:'',
      W_Height:'',
      M_Height:'',
      W_Wear:'',
      M_Wear:'',
      W_Pose:'',
      FirstSee:'',
      M_Style:'',
      W_Style:'',
      LikeAge:'',
      FaceType:'',
      W_MyStyle:'',
      M_myStyle:'',

      d1:'0',
      d2:'0',
      d3:'0',
      d4:'0',
      d5:'0',
      d0:'0',
      isD1:false,
      isD2:false,
      isD3:false,
      isD4:false,
      isD5:false,
      isD0:false,
      
      h0:'0',
      h1:'0',
      h2:'0',
      h3:'0',
      h4:'0',
      h5:'0',
      h6:'0',
      h7:'0',
      h8:'0',
      h9:'0',
      h10:'0',
      h11:'0',
      h12:'0',
      h13:'0',
      isH0:false,
      isH1:false,
      isH2:false,
      isH3:false,
      isH4:false,
      isH5:false,
      isH6:false,
      isH7:false,
      isH8:false,
      isH9:false,
      isH10:false,
      isH11:false,
      isH12:false,
      isH13:false,
      
      
    };
  }
//----跳頁function----
    _navigatePush(nextRoute) {
        this.props.navigator.push(nextRoute);
    }
    _navigateReplace(nextRoute){
        this.props.navigator.replace(nextRoute);
    }

  //註冊後登入
  _login(Account,Password,nextHome) {
    var userData="";
    var self=this;
    fetch('http://163.17.135.185/Hearting/api/Memberapi/Read', {
      method: 'POST',
      headers: {
      //'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json',
    'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "Account":Account,
        "Password": Password
      })
    }).then((response) => response.json())
    .then((responseData) => {
      console.log(JSON.stringify(responseData));
      userData=JSON.stringify(responseData.user);
      console.log(responseData.user);
    //   AlertIOS.alert(
    //     "POST Response",
    //     "Response Body -> " + JSON.stringify(responseData)
    // )
      AsyncStorage.multiSet([
        ["userData", userData]
      ])
      // self.setState({ animating: false, loaded: true });
      return (
      <ActivityIndicator
        animating={this.state.animating}
        style={[styles.centering, {height: 80}]}
        size="large"
      />
      );
    })
    .done(()=>{
      this._navigateReplace(nextHome);
    });
  }

  

  //性別radioButton
  _sexRadioButton(sex) {
    if(sex === 'boy'){
      this.setState({
        isGirl:false,
        isBoy:true,
        Sex:'男'
      });
    }else{
      this.setState({
        isBoy:false,
        isGirl:true,
        Sex:'女'        
      })
    }
  }

  //性向radioButton
  _aptitudeRadioButton(aptitude){
    if(aptitude === 'loveBoy'){
      this.setState({
        isLoveBoy:true,
        isLoveGril:false,
        isLoveBoth:false,
        Aptitude:'男'
      })
    }else if(aptitude === 'loveGirl'){
      this.setState({
        isLoveBoy:false,
        isLoveGril:true,
        isLoveBoth:false,
        Aptitude:'女'
      })
    }else{
      this.setState({
        isLoveBoy:false,
        isLoveGril:false,
        isLoveBoth:true,
        Aptitude:'雙性',        
      })
    }
  }

  //血型radioButton
  _bloodRadioButton(blood){
    if(blood === 'A'){
      this.setState({
        isA:true,
        isB:false,
        isO:false,
        isAB:false,
        Blood:'A'
      });
    }else if(blood === 'B'){
      this.setState({
        isA:false,
        isB:true,
        isO:false,
        isAB:false,
        Blood:'B'        
      });
    }else if(blood === 'O'){
      this.setState({
        isA:false,
        isB:false,
        isO:true,
        isAB:false,
        Blood:'O'        
      });
    }else{
      this.setState({
        isA:false,
        isB:false,
        isO:false,
        isAB:true,
        Blood:'AB'        
      });
    }
  }
  //判斷星座
  _Constellation(date){
    var birthday=new Date(date);
    var year=birthday.getFullYear();
    var mouth=birthday.getMonth();
    var day=birthday.getDay();
    this.setState({Age:2017-year});
    if(mouth === 0){
      if(day < 21){
        this.setState({Constellation:"摩羯座"});
      }
      else{
        this.setState({Constellation:"水瓶座"});        
      }
    }else if(mouth === 1){
      if(day < 21){
        this.setState({Constellation:"水瓶座"});
      }
      else{
        this.setState({Constellation:"雙魚座"});        
      }

    }else if(mouth === 2){
      if(day < 21){
        this.setState({Constellation:"雙魚座"});
      }
      else{
        this.setState({Constellation:"牡羊座"});        
      }

    }else if(mouth === 3){
      if(day < 21){
        this.setState({Constellation:"牡羊座"});
      }
      else{
        this.setState({Constellation:"金牛座"});        
      }
      
    }else if(mouth === 4){
      if(day < 21){
        this.setState({Constellation:"金牛座"});
      }
      else{
        this.setState({Constellation:"雙子座"});        
      }
      
    }else if(mouth === 5){
      if(day < 21){
        this.setState({Constellation:"雙子座"});
      }
      else{
        this.setState({Constellation:"巨蟹座"});        
      }
    }else if(mouth === 6){
      if(day < 21){
        this.setState({Constellation:"巨蟹座"});
      }
      else{
        this.setState({Constellation:"獅子座"});        
      }
    }else if(mouth === 7){
      if(day < 21){
        this.setState({Constellation:"獅子座"});
      }
      else{
        this.setState({Constellation:"處女座"});        
      }
    }else if(mouth === 8){
      if(day < 21){
        this.setState({Constellation:"處女座"});
      }
      else{
        this.setState({Constellation:"天枰座"});        
      }
    }else if(mouth === 9){
      if(day < 21){
        this.setState({Constellation:"天枰座"});
      }
      else{
        this.setState({Constellation:"天蠍座"});        
      }
    }else if(mouth === 10){
      if(day < 21){
        this.setState({Constellation:"天蠍座"});
      }
      else{
        this.setState({Constellation:"射手座"});        
      }
    }else if(mouth === 11){
      if(day < 21){
        this.setState({Constellation:"射手座"});
      }
      else{
        this.setState({Constellation:"摩羯座"});        
      }
    }
  }

  //註冊post
  _register(nextRoute){
    var userData="";
    var self=this;
    console.log(self.state.Constellation);
    // this._Constellation(this.state.Birthday);
    fetch('http://163.17.135.185/Hearting/api/Memberapi/Create', {
      method: 'POST',
      headers: {
      //'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "master":{
          "Account":self.state.Account,
          "Password": self.state.Password,
          "Name": self.state.Name, 
          "Email": self.state.Email, 
          "Age": this.state.Age, 
          "Birthday": self.state.Birthday, 
          "Sex": self.state.Sex, 
          "Aptitude": self.state.Aptitude,
          "Constellation":self.state.Constellation,
          "Blood":self.state.Blood,
          "Height":self.state.Height,
          "Weight":self.state.Weight,
          "Address":self.state.Address,
          isLove:"單身"
          // Account: 'test123444',
          // Password: '1234444', 
          // Name: '何彥儒', 
          // Email: 'c954127@gmail.com', 
          // Age: 20, 
          // Birthday: '1997/03/12', 
          // Sex: '男', 
          // Aptitude: '女',
          // Constellation:'雙魚座',
          // Blood:'O',
          // Height:'185',
          // Weight:'70',
          // Address:'台中市',
         },
        detail: {
          Account:self.state.Account,
          Decorations:self.state.Decorations,
          M_Hairstyle:self.state.M_Hairstyle, 
          W_Hairstyle:self.state.W_Hairstyle,
          isMakeup:self.state.isMakeup,
          Eyes:self.state.Eyes,
          Eyelid:self.state.Eyelid,
          Eyebrow:self.state.Eyebrow,
          Chest:self.state.Chest,
          Stature:self.state.Stature,
          W_Height:self.state.W_Height,
          M_Height:self.state.M_Height,
          W_Wear:self.state.W_Wear,
          M_Wear:self.state.M_Wear,
          W_Pose:self.state.W_Pose,
          FirstSee:self.state.FirstSee,
          M_Style:self.state.M_Style,
          W_Style:self.state.W_Style,
          LikeAge:self.state.LikeAge,
          FaceType:self.state.FaceType,
          W_MyStyle:self.state.W_MyStyle,
          M_myStyle:self.state.M_myStyle,

          }

      })
    }).then((response) => response.json())
    .then((responseData) => {
      console.log(JSON.stringify(responseData));
      userData=JSON.stringify(responseData.user);
      console.log(responseData.user);
    //   AlertIOS.alert(
    //     "POST Response",
    //     "Response Body -> " + JSON.stringify(responseData)
    // )
      AsyncStorage.multiSet([
        ["userData", userData]
      ])
      return (
      <ActivityIndicator
        animating={this.state.animating}
        style={[styles.centering, {height: 80}]}
        size="large"
      />
      );
    })
    .done(()=>{
      self._login(self.state.Account,self.state.Password,nextRoute);
    });
  }

  //檢查帳號重複
  _checkAccount(account){
    var self=this;
    fetch('http://163.17.135.185/Hearting/api/Memberapi/QuOther', {
      method: 'POST',
      headers: {
      //'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json',
    'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        apiname  : 'CheckAccount',
        postdata : {Account : account}
      })
    }).then((response) => response.json())
    .then((responseData) => {
      console.log(JSON.stringify(responseData));
      if(responseData.result.status==="1"){
        AlertIOS.alert(
          "此帳號已被使用"
        );
      }
    
    })
    .done();
  }

  //檢查密碼
  _checkPassword(password){
    if(password.Password !== this.state.Password){
      this.setState({checkPassword:"密碼不一致"});
    }
    else{
      this.setState({checkPassword:''});
    }
  }

  //我的類型
  _myStyle(){
    if (this.state.isBoy) {
      return (
        <RadioForm
          radio_props={M_myStyle}
          initial={false}
          buttonColor="black"
          buttonSize={12}
          buttonOuterSize={25}
          labelHorizontal={true}
          onPress={(value) => {this.setState({W_MyStyle:value})}}
        />
      );
    } else {
      return (
        <RadioForm
          radio_props={W_myStyle}
          initial={false}
          buttonColor="black"
          buttonSize={12}
          buttonOuterSize={25}                
          labelHorizontal={true}
          onPress={(value) => {this.setState({W_MyStyle:value})}}
        />
      );
    }
  }

  //喜歡類型
  _likeStyle(){
    if (this.state.isGirl) {
      return (
        <RadioForm
          radio_props={M_myStyle}
          initial={false}
          buttonColor="black"
          buttonSize={12}
          buttonOuterSize={25}
          labelHorizontal={true}
          onPress={(value) => {this.setState({M_Style:value})}}
        />
      );
    } else {
      return (
        <RadioForm
          radio_props={W_myStyle}
          initial={false}
          buttonColor="black"
          buttonSize={12}
          buttonOuterSize={25}
          labelHorizontal={true}
          onPress={(value) => {this.setState({W_Style:value})}}
        />
      );
    }
  }

  //喜歡臉型
  _faceStyle(){
    return (
      <RadioForm
        radio_props={faceType}
        initial={false}
        buttonColor="black"
          buttonSize={12}
          buttonOuterSize={25}
        labelHorizontal={true}
        onPress={(value) => {this.setState({FaceType:value})}}
      />
    );
  }

   //喜歡年齡
  _likeAge(){
    return (    
      <RadioForm
        radio_props={likeAge}
        initial={false}
        buttonColor="black"
          buttonSize={12}
          buttonOuterSize={25}
        labelHorizontal={true}
        onPress={(value) => {this.setState({LikeAge:value})}}
      />
    );
  }

  //喜歡穿著
  _wear(){
    if (!this.state.isBoy) {
      return (
        <RadioForm
          radio_props={M_wear}
          initial={false}
          buttonColor="black"
          buttonSize={12}
          buttonOuterSize={25}
          labelHorizontal={true}
          onPress={(value) => {this.setState({M_Wear:value})}}
        />
      );
    } else {
      return (
        <RadioForm
          radio_props={W_wear}
          initial={false}
          buttonColor="black"
          buttonSize={12}
          buttonOuterSize={25}
          labelHorizontal={true}
          onPress={(value) => {this.setState({W_Wear:value})}}
        />
      );
    }
  }

  //喜歡身高
  _height(){
    if (this.state.isBoy) {
      return (
        <RadioForm
          radio_props={W_height}
          initial={false}
          buttonColor="black"
          buttonSize={12}
          buttonOuterSize={25}
          labelHorizontal={true}
          onPress={(value) => {this.setState({W_Height:value})}}
        />
      );
    } else {
      return (
        <RadioForm
          radio_props={M_height}
          initial={false}
          buttonColor="black"
          buttonSize={12}
          buttonOuterSize={25}
          labelHorizontal={true}
          onPress={(value) => {this.setState({M_Height:value})}}
        />
      );
    }
  }

//喜歡髮型
  _hairStyle(){
    if (this.state.isBoy) {
      return (
        <RadioForm
          radio_props={W_hairStyle}
          initial={false}
          buttonColor="black"
          buttonSize={12}
          buttonOuterSize={25}
          labelHorizontal={true}
          onPress={(value) => {this.setState({W_Hairstyle:value})}}
        />
      );
    } else {
      return (
        <RadioForm
          radio_props={M_hairStyle}
          initial={false}
          buttonColor="black"
          buttonSize={12}
          buttonOuterSize={25}
          labelHorizontal={true}
          onPress={(value) => {this.setState({M_Hairstyle:value})}}
        />
      );
    }
  }

  _makeup(){
    return (    
      <RadioForm
        radio_props={isMakeup}
        initial={false}
        buttonColor="black"
          buttonSize={12}
          buttonOuterSize={25}
        labelHorizontal={true}
        onPress={(value) => {this.setState({isMakeup:value})}}
      />
    );
  }

  //喜歡眼睛
  _eye(){
    return (    
      <RadioForm
        radio_props={eyes}
        initial={false}
        buttonColor="black"
          buttonSize={12}
          buttonOuterSize={25}
        labelHorizontal={true}
        onPress={(value) => {this.setState({Eyes:value})}}
      />
    );
  }

  //喜歡眼皮
  _eyelid(){
    return (    
      <RadioForm
        radio_props={eyelid}
        initial={false}
        buttonColor="black"
          buttonSize={12}
          buttonOuterSize={25}
        labelHorizontal={true}
        onPress={(value) => {this.setState({Eyelid:value})}}
      />
    );
  }

  //喜歡眉毛
  _eyebrow(){
    return (    
      <RadioForm
        radio_props={eyebrow}
        initial={false}
        buttonColor="black"
          buttonSize={12}
          buttonOuterSize={25}
        labelHorizontal={true}
        onPress={(value) => {this.setState({Eyebrow:value})}}
      />
    );
  }

  //喜歡身材
   _body(){
    if (this.state.isBoy) {
      return (
        <RadioForm
          radio_props={chest}
          initial={false}
          buttonColor="black"
          buttonSize={12}
          buttonOuterSize={25}
          labelHorizontal={true}
          onPress={(value) => {this.setState({Chest:value})}}
        />
      );
    } else {
      return (
        <RadioForm
          radio_props={stature}
          initial={false}
          buttonColor="black"
          buttonSize={12}
          buttonOuterSize={25}
          labelHorizontal={true}
          onPress={(value) => {this.setState({Stature:value})}}
        />
      );
    }
  }

  //喜歡飾品
  _decoration(){
    return (    
      
      <View>
        <CheckBox
            label="戴眼鏡"
            size={30}
            checked={this.state.isD0}
            onPress={()=>this._checkButtonDecoration(0,this.state.isD0)}
          />
          <CheckBox
            label="放大片"
            size={30}
            checked={this.state.isD1}
            onPress={()=>this._checkButtonDecoration(1,this.state.isD1)}
          />
          <CheckBox
            label="項鍊"
            size={30}
            checked={this.state.isD2}
            onPress={()=>this._checkButtonDecoration(2,this.state.isD2)}
          /><CheckBox
            label="耳環"
            size={30}
            checked={this.state.isD3}
            onPress={()=>this._checkButtonDecoration(3,this.state.isD3)}
          /><CheckBox
            label="髮帶"
            size={30}
            checked={this.state.isD4}
            onPress={()=>this._checkButtonDecoration(4,this.state.isD4)}
          /><CheckBox
            label="帽子"
            size={30}
            checked={this.state.isD5}
            onPress={()=>this._checkButtonDecoration(5,this.state.isD5)}
          />
      </View>
    );
  }

  //checkbox裝飾品多選
  _checkButtonDecoration(checkbox,bool){
    if(bool){
      if(checkbox === 0){
        this.setState({isD0:false,d0:'0'});
      }else if(checkbox === 1){
        this.setState({isD1:false,d1:'0'});
      }else if(checkbox === 2){
        this.setState({isD2:false,d2:'0'});
      }else if(checkbox === 3){
        this.setState({isD3:false,d3:'0'});
      }else if(checkbox === 4){
        this.setState({isD4:false,d4:'0'});
      }else if(checkbox === 5){
        this.setState({isD5:false,d5:'0'});
      }
    }
    else{
      if(checkbox === 0){
        this.setState({isD0:true,d0:'1'});
      }else if(checkbox === 1){
        this.setState({isD1:true,d1:'1'});
      }else if(checkbox === 2){
        this.setState({isD2:true,d2:'1'});
      }else if(checkbox === 3){
        this.setState({isD3:true,d3:'1'});
      }else if(checkbox === 4){
        this.setState({isD4:true,d4:'1'});
      }else if(checkbox === 5){
        this.setState({isD5:true,d5:'1'});
      }
    }
    var str=this.state.d0+this.state.d1+this.state.d2+this.state.d3+this.state.d4+this.state.d5;
    this.setState({Decorations:str});
    console.log(this.state.Decorations);
  }

  //喜歡拍照姿勢
  _pose(){
    return (    
      <RadioForm
        radio_props={pose}
        initial={false}
        buttonColor="black"
          buttonSize={12}
          buttonOuterSize={25}
        labelHorizontal={true}
        onPress={(value) => {this.setState({W_Pose:value})}}
      />
    );
  }

  //第一眼看的部位
  _firstSee(){
    return (    
      <RadioForm
        radio_props={firstSee}
        initial={false}
        buttonColor="black"
          buttonSize={12}
          buttonOuterSize={25}
        labelHorizontal={true}
        onPress={(value) => {this.setState({FirstSee:value})}}
      />
    );
  }


  //註冊content
  render() {
    
    const nextHome = {
      component: home,
      title: '註冊',
      passProps: { myProp: 'bar',title:'123' }
    };
    return (
      <View style={styles.container}>
        <Image source={require('../images/home.png')} style={styles.backgroundImage}>
          <View style={styles.bigview}>
            <Carousel 
                ref={(carousel) => { this._carousel = carousel; } } 
                sliderWidth={width} 
                itemWidth={width}              
                directionalLockEnabled={true}
                centerContent={true}>
              <View style={styles.slide1} >

                <Text style={styles.loginText}>
                    Register
                </Text>
                <TextInput 
                    placeholder="UserName..."          
                    placeholderTextColor="gray"
                    style={styles.textInput}
                    onChangeText={(Account) => this.setState({Account})}
                    value={this.state.Account}
                    autoCapitalize="none"
                    onEndEditing={() => this._checkAccount(this.state.Account)}
                />
                <View style={styles.space}></View>
                <TextInput 
                    placeholder="Password..."          
                    placeholderTextColor="gray"
                    style={styles.textInput}
                    onChangeText={(Password) => this.setState({Password})}
                    value={this.state.Password}
                    autoCapitalize="none"    
                    secureTextEntry={true}                
                />
                <View style={styles.space}></View>            
                <TextInput 
                    placeholder="Check Password..."          
                    placeholderTextColor="gray"
                    style={styles.textInput}
                    autoCapitalize="none"
                    onChangeText={(Password) => this._checkPassword({Password})} 
                    secureTextEntry={true}                                       
                />
                
                <View style={styles.space}>
                  <Text>{this.state.checkPassword}</Text>
                </View>            
                
                <View style={styles.button}>
                    <Button onPress={() => { this._carousel.snapToNext(); } }
                        title=">"
                        color="white"
                        >
                    </Button>
                </View>
              </View>
              <View style={styles.slide1} >
                <Text style={styles.loginText}>
                        Register
                    </Text>
                    <TextInput 
                        placeholder="姓名..."          
                        placeholderTextColor="gray"
                        style={styles.textInput}
                        onChangeText={(Name) => this.setState({Name})}
                        value={this.state.Name}
                    />
                    <View style={styles.space}></View>
                    
                    <DatePicker
                      style={{width: 200}}
                      date={this.state.Birthday}
                      mode="date"
                      placeholder="placeholder"
                      format="YYYY/MM/DD"
                      minDate="1900/01/01"
                      maxDate="2016/06/01"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      // iconSource={require('./google_calendar.png')}
                      onDateChange={(Birthday) => {this.setState({Birthday:Birthday});this._Constellation(Birthday)}}
                    />
                    <View style={styles.space}><Text> {this.state.Constellation}</Text></View>            
                    <TextInput 
                        placeholder="信箱..."          
                        placeholderTextColor="gray"
                        style={styles.textInput}
                        onChangeText={(Email) => this.setState({Email})}
                        value={this.state.Email}
                        autoCapitalize="none"                        
                    />
                    <View style={styles.radioView}>
                      <CheckBox
                        label="男"
                        size={30}
                        checked={this.state.isBoy}
                        onPress={() => this._sexRadioButton('boy')}
                        uncheckedIconName="radio-button-unchecked"
                        checkedIconName="radio-button-checked"
                      />
                      <CheckBox
                        label="女"
                        size={30}
                        checked={this.state.isGirl}
                        onPress={() => this._sexRadioButton('gril')}
                        uncheckedIconName="radio-button-unchecked"
                        checkedIconName="radio-button-checked"
                      />
                    </View>
                    <TextInput 
                        placeholder="身高..."          
                        placeholderTextColor="gray"
                        style={styles.textInput}
                        onChangeText={(Height) => this.setState({Height})}
                        value={this.state.Height}
                    />
                    <View style={styles.space}></View>                  
                    <TextInput 
                        placeholder="體重..."          
                        placeholderTextColor="gray"
                        style={styles.textInput}
                        onChangeText={(Weight) => this.setState({Weight})}
                        value={this.state.Weight}
                    />
                    <View style={styles.space}></View>                                
                    <TextInput 
                        placeholder="居住城市..."          
                        placeholderTextColor="gray"
                        style={styles.textInput}
                        onChangeText={(Address) => this.setState({Address})}
                        value={this.state.Address}
                    />
                    <View style={styles.radioView}>
                      <CheckBox
                        label="男"
                        size={30}
                        checked={this.state.isLoveBoy}
                        onPress={() => this._aptitudeRadioButton('loveBoy')}
                        uncheckedIconName="radio-button-unchecked"
                        checkedIconName="radio-button-checked"
                        onChangeText={() => this.setState({})}
                      />
                      <CheckBox
                        label="女"
                        size={30}
                        checked={this.state.isLoveGril}
                        onPress={() => this._aptitudeRadioButton('loveGirl')}
                        uncheckedIconName="radio-button-unchecked"
                        checkedIconName="radio-button-checked"
                      />
                      <CheckBox
                        label="雙性"
                        size={30}
                        checked={this.state.isLoveBoth}
                        onPress={() => this._aptitudeRadioButton('loveBoth')}
                        uncheckedIconName="radio-button-unchecked"
                        checkedIconName="radio-button-checked"
                      />
                    </View>
                    <View style={styles.radioView}>
                      <CheckBox
                        label="Ａ"
                        size={30}
                        checked={this.state.isA}
                        onPress={() => this._bloodRadioButton('A')}
                        uncheckedIconName="radio-button-unchecked"
                        checkedIconName="radio-button-checked"
                      />
                      <CheckBox
                        label="B"
                        size={30}
                        checked={this.state.isB}
                        onPress={() => this._bloodRadioButton('B')}
                        uncheckedIconName="radio-button-unchecked"
                        checkedIconName="radio-button-checked"
                      />
                      <CheckBox
                        label="O"
                        size={30}
                        checked={this.state.isO}
                        onPress={() => this._bloodRadioButton('O')}
                        uncheckedIconName="radio-button-unchecked"
                        checkedIconName="radio-button-checked"
                      />
                      <CheckBox
                        label="AB"
                        size={30}
                        checked={this.state.isAB}
                        onPress={() => this._bloodRadioButton('AB')}
                        uncheckedIconName="radio-button-unchecked"
                        checkedIconName="radio-button-checked"
                      />
                    </View>
                    <View style={styles.button}>
                        <Button onPress={() => { this._carousel.snapToNext(); } }
                            title=">"
                            color="white"
                            >
                        </Button>
                    </View>
              </View>   
              <View style={styles.slide1} >
                <ScrollView style={styles.scrollView}>
                  <Text style={styles.loginText}>
                      問卷
                  </Text>
                  <Text style={styles.columnText}>我的類型</Text>  
                  {this._myStyle()}
                  <Text style={styles.columnText}>喜歡類型</Text>  
                  {this._likeStyle()}                  
                  <Text style={styles.columnText}>喜歡臉型</Text>  
                  {this._faceStyle()}
                  <Text style={styles.columnText}>喜歡年齡</Text> 
                  {this._likeAge()} 
                  <Text style={styles.columnText}>喜歡穿著</Text>  
                  {this._wear()}
                  <Text style={styles.columnText}>喜歡身高</Text>  
                  {this._height()}
                  <Text style={styles.columnText}>喜歡髮型</Text> 
                  {this._hairStyle()} 
                  <Text style={styles.columnText}>化妝</Text>  
                  {this._makeup()}
                  <Text style={styles.columnText}>喜歡眼睛</Text>  
                  {this._eye()}
                  <Text style={styles.columnText}>喜歡眼皮</Text>  
                  {this._eyelid()}
                  <Text style={styles.columnText}>喜歡眉毛</Text>  
                  {this._eyebrow()}
                  <Text style={styles.columnText}>喜歡身材</Text>  
                  {this._body()}
                  <Text style={styles.columnText}>喜歡飾品</Text>  
                  {this._decoration()}
                  <Text style={styles.columnText}>喜歡拍照姿勢</Text>  
                  {this._pose()}
                  <Text style={styles.columnText}>第一眼看的部位</Text>  
                  {this._firstSee()}    
                  <View style={styles.button}>
                        <Button onPress={() => { this._register(nextHome); } }
                            title="登入"
                            color="white"
                            >
                        </Button>
                    </View>              
                </ScrollView>
              </View>                   
            </Carousel>
          </View>
        </Image>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',   
    flexDirection:'row',
  },
  loginText: {
    fontSize: 40,
    textAlign: 'center',
    margin: 20,
    fontFamily: 'Euphemia UCAS',
    backgroundColor:'rgba(0,0,0,0)',
  },
  
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  backgroundImage: {
    flex: 1,
    //resizeMode: 'cover', // or 'stretch'
    width: null,
    height: null,
    justifyContent:'center',
    alignItems:'center',
    // flexDirection:'row'
  },
  bigview:{
    justifyContent:'center',
    flex:1,
    flexDirection:'row',
    alignItems:'center',
    
  },
  textInput:{
    // width:200,
    height: 30,
    borderColor: 'black', 
    borderWidth: 1,
    paddingLeft:5,
    fontFamily: 'Euphemia UCAS',
    
  },
  space:{
    height:20,
  },
  button:{
    paddingTop:3,
    paddingBottom:3,
    paddingLeft:10,
    paddingRight:10,
    backgroundColor:'black',
    marginTop:20,
    
  },
  slide1:{
    justifyContent:'center',
    padding:width*0.15,
    width:width,
    height:600,
    alignItems:'center',
  },
  radioView:{
    // flex:1,
    flexDirection:'row',
  },
  content:{
    justifyContent:'center',
    flex:1,
    flexDirection:'row',    
  },
  columnText:{
    fontSize:20,
    marginRight:10,
    marginTop:5
  },
  scrollView:{
    padding:width*0.12,
    width:width,
    height:600,
  },
});


